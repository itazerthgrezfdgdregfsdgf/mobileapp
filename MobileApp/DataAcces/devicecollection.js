var mongoose = require('mongoose');
var deviceSchema = new mongoose.Schema({devicename: String,
  devicetype: String,
  devicedescription: String,
  imagebyt:String,
  deviceregistrations: {from:Date,until:Date,by:String}
  });
  module.exports = mongoose.model('devices', deviceSchema)
